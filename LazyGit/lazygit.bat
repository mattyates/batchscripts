@echo off
echo Enter "c" to commit, or "u" to update

REM LazyGit configuration for source-control
set BRANCH="origin"
set COMMIT_MSG="Auto-committed on %date%, via LazyGit :zzz: Script"

:GITLOGIC
set ACTION=
set /P ACTION=Action: %=%
if "%ACTION%"=="c" (
    REM Below are commands specific to repositories with a custom-named .git
    REM git --git-dir=.gitcustom commit -m %COMMIT_MSG%
	REM git --git-dir=.gitcustom pull %BRANCH%
	REM git --git-dir=.gitcustom push %BRANCH%
    git add -A
	git commit -am %COMMIT_MSG%
	git pull %BRANCH%
	git push %BRANCH%
)
if "%ACTION%"=="u" (
    REM git --git-dir=.gitcustom pull %BRANCH%
    git pull %BRANCH%
)
if "%ACTION%"=="exit" exit /b
goto GITLOGIC