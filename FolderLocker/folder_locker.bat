:START
    @ECHO OFF
    cls
    SetLocal enabledelayedexpansion enabledelayedexpansion enableextensions

    :: App. Window Name
    title Folder Locker
    color a

    :: What to do logic...
    goto WHERETO

:WHERETO
    :: If Control Panel Registry key exists, Locker dir exists -> jump to unlock
    if EXIST "Control Panel.{21EC2020-3AEA-1069-A2DD-08002B30309D}" goto UNLOCK
    :: If Locker dir doesn't exist -> mkdir Locker and exit, allowing user to transfer files first
    if NOT EXIST Locker goto MDLOCKER

:CONFIRM
    :: Confirm lock first
    echo Are you sure u want to Lock the folder(Y/N)
    set/p "cho=>"
    if %cho%==Y goto LOCK
    if %cho%==y goto LOCK
    if %cho%==n goto END
    if %cho%==N goto END
    echo Invalid choice.
    goto CONFIRM

:LOCK
    :: Ensure credentials file is g2g
    ATTRIB -R credentials.txt>NUL
    (CD .>credentials.txt)2>NUL

    :: Collect password from user and hide string during input
    powershell -Command $pword = read-host "Enter a password for Locker" -AsSecureString ; ^
        $BSTR=[System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($pword) ; ^
            [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($BSTR) > .tmp.txt 
    set /p password=<.tmp.txt & del .tmp.txt

    :: encrypt the password before storage by calling Encrypt.bat and providing password
    set encinp=%password%
    call :ENCRYPT

    :: Save password to file
    echo %EncryptOut% >credentials.txt

    :: Hide credentials file
    attrib +h +s credentials.txt

    :: Rename locker to special control panel folder
    ren Locker "Control Panel.{21EC2020-3AEA-1069-A2DD-08002B30309D}"
    :: Give permissions to the folder
    attrib +h +s "Control Panel.{21EC2020-3AEA-1069-A2DD-08002B30309D}"

    echo Folder locked

    set /p LOCK_CONT=Hit ENTER to continue...
    goto End

:UNLOCK
    :: Read previously saved password
    attrib -h -s credentials.txt
    set /p SavedPass=<credentials.txt

    :: Trim whitespace from SavedPass
    call :TRIM Actual %SavedPass%

    :: Decrypt the actual password
    set decinp=%Actual%
    call :DECRYPT

    :: Get user input password for comparison
    powershell -Command $pword = read-host "Enter Locker password" -AsSecureString ; ^
        $BSTR=[System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($pword) ; ^
            [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($BSTR) > .tmp.txt 
    set /p pass=<.tmp.txt & del .tmp.txt
    if NOT "%pass%" == "%DecryptOut%" goto FAIL

    :: Restore folder on successful pass
    attrib -h -s "Control Panel.{21EC2020-3AEA-1069-A2DD-08002B30309D}"
    ren "Control Panel.{21EC2020-3AEA-1069-A2DD-08002B30309D}" Locker

    :: Delete credentials.txt
    del credentials.txt

    echo Folder Unlocked successfully
    set /p UNLOCK_CONT=Hit ENTER to continue...
    goto End

:FAIL
    echo Invalid Password.
    ::echo Decrypt Output: '%DecryptOut%'
    ::echo Password Input: '%pass%'
    set /p FAIL_CONT=Hit ENTER to continue...
    goto UNLOCK

:MDLOCKER
    md Locker
    echo Locker created successfully
    set /p MDLOCKER_CONT=Move your files into the Locker folder and hit ENTER when you are finished...
    goto LOCK

:TRIM 
    :: Trim trailing and leading whitespace from string
    set Params=%*
    for /f "tokens=1*" %%a in ("!Params!") do set %1=%%b
    exit /b

:ENCRYPT
    (set CHAR[a]=UDFM45) & (set CHAR[b]=H21DGF) & (set CHAR[c]=FDH56D) & (set CHAR[d]=FGS546) & (set CHAR[e]=JUK4JH)
    (set CHAR[f]=ERG54S) & (set CHAR[g]=T5H4FD) & (set CHAR[h]=RG641G) & (set CHAR[i]=RG4F4D) & (set CHAR[j]=RT56F6)
    (set CHAR[k]=VCBC3B) & (set CHAR[l]=F8G9GF) & (set CHAR[m]=FD4CJS) & (set CHAR[n]=G423FG) & (set CHAR[o]=F45GC2)
    (set CHAR[p]=TH5DF5) & (set CHAR[q]=CV4F6R) & (set CHAR[r]=XF64TS) & (set CHAR[s]=X78DGT) & (set CHAR[t]=TH74SJ)
    (set CHAR[u]=BCX6DF) & (set CHAR[v]=FG65SD) & (set CHAR[w]=4KL45D) & (set CHAR[x]=GFH3F2) & (set CHAR[y]=GH56GF)
    (set CHAR[z]=45T1FG) & (set CHAR[1]=D4G23D) & (set CHAR[2]=GB56FG) & (set CHAR[3]=SF45GF) & (set CHAR[4]=P4FF12)
    (set CHAR[5]=F6DFG1) & (set CHAR[6]=56FG4G) & (set CHAR[7]=USGFDG) & (set CHAR[8]=FKHFDG) & (set CHAR[9]=IFGJH6)
    (set CHAR[0]=87H8G7) & (set CHAR[@]=G25GHF) & (set CHAR[#]=45FGFH) & (set CHAR[$]=75FG45) & (set CHAR[*]=54GDH5)
    (set CHAR[(]=45F465) & (set CHAR[.]=HG56FG) & (set CHAR[,]=DF56H4) & (set CHAR[-]=F5JHFH) & (set CHAR[ ]=SGF4HF)
    (set CHAR[\]=45GH45) & (set CHAR[/]=56H45G)
    :: get string to encrypt from argument
    set Encrypt2=%encinp%
    set "EncryptOut="
    goto ENCRYPT2

:ENCRYPT2
    set echar=%Encrypt2:~0,1%
    set Encrypt2=%Encrypt2:~1%
    set EncryptOut=%EncryptOut%!CHAR[%echar%]!
    if not "%Encrypt2%"=="" goto ENCRYPT2
    set estring=%EncryptOut%
    exit /b

:DECRYPT
    (set CHAR[UDFM45]=a) & (set CHAR[H21DGF]=b) & (set CHAR[FDH56D]=c) & (set CHAR[FGS546]=d) & (set CHAR[JUK4JH]=e)
    (set CHAR[ERG54S]=f) & (set CHAR[T5H4FD]=g) & (set CHAR[RG641G]=h) & (set CHAR[RG4F4D]=i) & (set CHAR[RT56F6]=j)
    (set CHAR[VCBC3B]=k) & (set CHAR[F8G9GF]=l) & (set CHAR[FD4CJS]=m) & (set CHAR[G423FG]=n) & (set CHAR[F45GC2]=o)
    (set CHAR[TH5DF5]=p) & (set CHAR[CV4F6R]=q) & (set CHAR[XF64TS]=r) & (set CHAR[X78DGT]=s) & (set CHAR[TH74SJ]=t)
    (set CHAR[BCX6DF]=u) & (set CHAR[FG65SD]=v) & (set CHAR[4KL45D]=w) & (set CHAR[GFH3F2]=x) & (set CHAR[GH56GF]=y)
    (set CHAR[45T1FG]=z) & (set CHAR[D4G23D]=1) & (set CHAR[GB56FG]=2) & (set CHAR[SF45GF]=3) & (set CHAR[P4FF12]=4)
    (set CHAR[F6DFG1]=5) & (set CHAR[56FG4G]=6) & (set CHAR[USGFDG]=7) & (set CHAR[FKHFDG]=8) & (set CHAR[IFGJH6]=9)
    (set CHAR[87H8G7]=0) & (set CHAR[G25GHF]=@) & (set CHAR[45FGFH]=#) & (set CHAR[75FG45]=$) & (set CHAR[54GDH5]=*)
    (set CHAR[45F465]=() & (set CHAR[HG56FG]=.) & (set CHAR[DF56H4]=,) & (set CHAR[F5JHFH]=-) & (set CHAR[SGF4HF]= )
    (set CHAR[45GH45]=\) & (set CHAR[56H45G]=/)
    :: get string from credentials.txt or passed from call
    set Decrypt2=%decinp%
    set "DecryptOut="
    goto DECRYPT2

:DECRYPT2
    set dchar=%Decrypt2:~0,6%
    set Decrypt2=%Decrypt2:~6%
    set DecryptOut=%DecryptOut%!CHAR[%dchar%]!
    if not "%Decrypt2%"=="" goto DECRYPT2
    set dstring=%DecryptOut%
    exit /b

:End