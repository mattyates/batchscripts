# Random Collection of Batch Scripts :)

Heres a general overview of each script (in their respective directory)

*  FolderLocker
    - This script gives the user a folder in which they can drop files they wish to hide. Using a CLI, the user can then lock the folder via password and the script will hide the folder in the user's computer until the script is ran again and the proper password is provided. **Good luck if you forget the password**
*  LazyGit
    - This script automates small git tasks. Its pretty straight forward, and it was made because my lazy self hated adding, commiting, and pushing every git change
*  MatrixScreen
    - This script was a horrible attempt at a matrix like screen on the CL